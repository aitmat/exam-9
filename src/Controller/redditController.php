<?php

namespace App\Controller;

use App\Entity\FavoritePost;
use App\Form\Reddit\MainSearchType;
use App\Model\Api\ApiContext;
use App\Model\Reddit\RedditHandler;
use App\Repository\FavoritePostRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class redditController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param RedditHandler $redditHandler
     * @param FavoritePostRepository $favoritePostRepository
     * @return Response
     * @throws \App\Model\Api\ApiException
     */
    public function indexAction(ApiContext $apiContext, Request $request, RedditHandler $redditHandler, FavoritePostRepository $favoritePostRepository)
    {
        $form = $this->createForm(MainSearchType::class);
        $form->handleRequest($request);
        $posts = $apiContext->redditMainPageQuery();


        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $sort = $redditHandler->generateArrayToReddit($data);
            $posts = $apiContext->redditMakeQuery($sort);
        }

        return $this->render('index/index.html.twig', [
            'form' => $form->createView(),
            'posts' => $posts['data']['children'],
        ]);
    }

    /**
     * @Route("/more/{id}", name="app_more_post")
     * @param ApiContext $apiContext
     * @param $id
     * @param FavoritePostRepository $favoritePostRepository
     * @return Response
     * @throws \App\Model\Api\ApiException
     */
    public function moreAction(ApiContext $apiContext, $id, FavoritePostRepository $favoritePostRepository)
    {
        $posts = $apiContext->redditGetMoreAboutPost($id);
        $count_favorite = $favoritePostRepository->countFavoriteAdded($id);

        return $this->render('index/more-post.html.twig', [
            'post' => $posts['data']['children'][0]['data'],
            'count' => count($count_favorite),
        ]);
    }

    /**
     * @Route("/add/favorites/{id}", name="app_add_favorites_post")
     * @param $id
     * @param ObjectManager $manager
     * @param FavoritePostRepository $favoritePostRepository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function addToFavoritesAction($id, ObjectManager $manager, FavoritePostRepository $favoritePostRepository)
    {
        if (!$this->getUser()){
            return $this->redirectToRoute('sign-in');
        }
        $user = $this->getUser();
        if ($favoritePostRepository->checkAlreadyAddedMyFavorite($id, $user->getId())){
            return $this->redirectToRoute('app_more_post', ['id' => $id]);
        }
        $post = new FavoritePost();
        $post->setIdPost($id);
        $post->setUser($this->getUser());
        $manager->persist($post);
        $manager->flush();

        return $this->redirectToRoute('app_more_post', ['id' => $id]);
    }

    /**
     * @Route("/favorite/posts", name="app_my_favorite_post")
     * @param ApiContext $apiContext
     * @param FavoritePostRepository $favoritePostRepository
     * @param Request $request
     * @return Response
     * @throws \App\Model\Api\ApiException
     */
    public function myFavoritesAction(ApiContext $apiContext, FavoritePostRepository $favoritePostRepository, Request $request)
    {
        $user = $this->getUser();
        if (!$user){
            return $this->redirectToRoute('sign-in');
        }
        $res = $favoritePostRepository->getMyFavoritePosts($user->getId());
        $param = null;
        /** @var FavoritePost $result */
        foreach ($res as $result){
            $param .= $result->getIdPost().",";
        }
        $posts = $apiContext->redditMyFavoritePosts($param);
        $posts2 = $posts['data']['children'];
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate($posts2, $request->query->getInt('page', 1), 4);

        return $this->render('index/my-favorite-posts.html.twig', [
            'pagination' => $pagination,
        ]);
    }


    /**
     * @Route("/communities/favorite", name="app_communities_favorites")
     * @param ApiContext $apiContext
     * @param FavoritePostRepository $favoritePostRepository
     * @param Request $request
     * @return Response
     * @throws \App\Model\Api\ApiException
     */
    public function favoritesCommunitiesAction(ApiContext $apiContext, FavoritePostRepository $favoritePostRepository, Request $request)
    {
        $user = $this->getUser();
        if (!$user){
            $this->redirectToRoute('sign-in');
        }
        $dql = $favoritePostRepository->getFavoritePosts();
        $param = null;
        /** @var FavoritePost $result */
        foreach ($dql as $result){
            $param .= $result['id_post'].",";
        }
        $posts = $apiContext->redditMyFavoritePosts($param);
        $posts2 = $posts['data']['children'];
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate($posts2, $request->query->getInt('page', 1), 4);

        return $this->render('index/favorite-posts.html.twig', [
            'pagination' => $pagination,
        ]);
    }
}
