<?php

namespace App\Model\Api;


class ApiContext extends AbstractApiContext
{
    const ENDPOINT_PING = '/ping';
    const ENDPOINT_CONCRETE_CLIENT = '/client/passport/{passport}/{email}';


    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function redditMakeQuery(array $data){
        return $this->makeQuery('/r/picture/search.json', self::METHOD_GET, $data);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function redditMainPageQuery(){
        return $this->makeQuery('/r/picture/search.json', self::METHOD_GET, ['q' => 'cat','sort' => 'hot','limit' => '16','type'=>'link']);
    }


    /**
     * @param $id
     * @return mixed
     * @throws ApiException
     */
    public function redditGetMoreAboutPost($id)
    {
        return $this->makeQuery('/api/info.json', self::METHOD_GET, ['id' => $id]);
    }

    /**
     * @param $param
     * @return mixed
     * @throws ApiException
     */
    public function redditMyFavoritePosts($param)
    {
        return $this->makeQuery('/api/info.json', self::METHOD_GET, ['id' => $param]);
    }
}